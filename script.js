/* 
    requests 
*/
let http = {};
http.quest = function (option, callback) {
  let url = option.url;
  let method = option.method;
  let data = option.data;
  let timeout = option.timeout || 0;
  let xhr = new XMLHttpRequest();
  timeout > 0 && (xhr.timeout = timeout);
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status >= 200 && xhr.status < 400) {
        var result = xhr.responseText;
        try {
          result = JSON.parse(xhr.responseText);
        } catch (e) {}
        callback && callback(null, result);
      } else {
        callback && callback("status: " + xhr.status);
      }
    }
  }.bind(this);
  xhr.open(method, url, true);
  if (typeof data === "object") {
    try {
      data = JSON.stringify(data);
    } catch (e) {}
  }
  xhr.send(data);
  xhr.ontimeout = function () {
    callback && callback("timeout");
    console.log("Timeout.");
  };
};
http.get = function (url, callback) {
  var option = url.url ? url : { url: url };
  option.method = "get";
  this.quest(option, callback);
};
http.post = function (option, callback) {
  option.method = "post";
  this.quest(option, callback);
};

const host = "https://cws.auckland.ac.nz/gas/api";
api = (route) => host + route;

/* 
    mixin 
*/

let mixin = {
  _version: "0.0.0",
  _shop_items: [],
  _filtered_items: [],
  _account: {
    status: 0,
  },
};

let mixin_handlers = {
  _version: [],
  _shop_items: [(value) => {}],
  _filtered_items: [(value) => {}],
  _account: [],
};

/* 
    router 
*/
let router = {
  default_page: "main",
  templates: {
    main: `
            <div id="main-content" class="content">
                <div id="welcome">
                    <div id="welcome-img">
                        <img src="non.jpg" />
                    </div>
                    <div id="welcome-msg">麒麟さんですか、象さんですか？</div>
                    <div class="version" style="margin-top: 0.5rem">
                        <span>Version </span>
                        <span id="main-version"></span>
                    </div>
                </div>
            </div>
        `,
    shop: `
            <div id="shop-content" class="content">
                <div id="search-bar">

                </div>
                <div id="search results">
                    <!--filled by scripts-->
                </div>
            </div>
        `,
    game: `
            <div id="game-content" class="content">
                <canvas id="gamepad"></canvas>
                <span>Play game.</span>
            </div>
        `,
    register: `
        <div id="register-content" class="content">
        <form class="regform" method="post">
            <div class="form-group input-group">
                <span class="input-group-icon">
                    <svg class="icon-ui" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 494.999 494.999" style="enable-background:new 0 0 494.999 494.999;" xml:space="preserve">
                        <path xmlns="http://www.w3.org/2000/svg" d="M15.71,12.71a6,6,0,1,0-7.42,0,10,10,0,0,0-6.22,8.18,1,1,0,0,0,2,.22,8,8,0,0,1,15.9,0,1,1,0,0,0,1,.89h.11a1,1,0,0,0,.88-1.1A10,10,0,0,0,15.71,12.71ZM12,12a4,4,0,1,1,4-4A4,4,0,0,1,12,12Z"/>
                    </svg>
                </span>
                <input name="username" type="text" class="form-control" placeholder="User Name" value="" required="required" autofocus="" autocomplete="username">
            </div>
            <div class="form-group input-group">
                <span class="input-group-icon">
                    <svg class="icon-ui" x="0px" y="0px" viewBox="0 0 24 24">
                        <path class="stroke" d="M2,4h20v16H2V4z"></path>
                        <path class="stroke" d="M2,8l10,5l10-5"></path>
                    </svg>
                </span>
                <input name="email" type="text" class="form-control" placeholder="Email Address" value="" required="required" autofocus="" autocomplete="email">
            </div>
            <div class="form-group input-group">
                <span class="input-group-icon">
                    <svg class="icon-ui" x="0px" y="0px" viewBox="0 0 24 24">
                        <path class="fill" d="M19,8V7c0-3.9-3.1-7-7-7S5,3.1,5,7v1h2V7c0-2.8,2.2-5,5-5s5,2.2,5,5v1H19"></path>
                        <path class="stroke" d="M3,9h18v14H3V9z"></path>
                        <circle class="fill" cx="8" cy="16" r="1"></circle>
                        <circle class="fill" cx="12" cy="16" r="1"></circle>
                        <circle class="fill" cx="16" cy="16" r="1"></circle>
                    </svg>
                </span>
                <input name="password" class="form-control" type="password" placeholder="Password" required="required" autocomplete="current-password">
            </div>
            
            <div class="rail">
                <div class="rail-content">
                </div>
                <div class="rail-toolbar">
                    <a href="#register">Register</a>
                </div>
            </div>
        
        
        <div class="form__actions">
            <div class="p-3 text-center m-t-3x">
                                    <button type="submit" class="btn btn--primary btn--block" onclick="this.disabled = true; this.form.submit();"><span class="btn__text">Log In</span><span class="btn-hover-effect"></span></button>
                <br>
                <br>
                Log In with <a href="/sso/">Single Sign-On</a> instead.
                                </div>
        </div>
    </form>
        </div>
    `,
    login: `
        <div id="login-content" class="content">
            Login
        </div>
    `,
    logout: `
        <div id="logout-content" class="content">
            Logout
        </div>
    `,
  },
  current_page: this.default_page,
};

window.onload = () => {
  /* init default page */
  //   document.getElementById("main-wrapper").innerHTML =
  //     router.templates[router.default_page];

  /* listen hash changes */
  window.addEventListener(
    "hashchange",
    (e) => {
      var hash = location.hash.split("|")[0];
      tag = hash.replace(/#/g, "");

      if (!(tag in router.templates)) {
        tag = router.default_page;
      } else if (
        (tag == "register" || tag == "login") &&
        !!mixin.account?.status
      ) {
        tag = "logout";
      }
      router.current_page = tag;

      console.log("switch to ", tag);

      document.getElementById("main-wrapper").innerHTML = router.templates[tag];

      if (typeof router["tag"] == "function") {
        router["tag"]();
      }
    },
    false
  );

  window.dispatchEvent(new HashChangeEvent("hashchange"));

  init();
  bind();

  mixin.account = { status: 0 };
};

function updateDOM(selector, path) {
  http.get(api(path), function (err, result) {
    if (err) {
      console.error("Failed to fetch " + path + ".");
    }
    document.querySelector(selector).innerHTML = result;
  });
}

/* navigtion events */
function init() {
  /* get logo */
  updateDOM("#logo a", "/Logo");

  http.get(api("/Version"), function (err, result) {
    if (err) {
      console.error("Failed to fetch " + path + ".");
    }
    mixin.version = result;
  });

  /* register mixin */
  Object.keys(mixin).forEach((key) => {
    Object.defineProperty(mixin, key.slice(1), {
      get: function () {
        console.log("get key " + key);
        return mixin[key];
      },
      set: function (value) {
        console.log("set key " + value);
        mixin_handlers[key].forEach((handler) => {
          handler(value);
        });
        mixin[key] = value;
      },
    });
  });
}

/* register version events */
function bind() {
  // version change
  mixin_handlers["_version"].push(function (value) {
    tmp = document.querySelector("#version");
    if (tmp) {
      tmp.innerHTML = value;
    }
    tmp = document.querySelector("#main-version");
    if (tmp) {
      tmp.innerHTML = value;
    }
  });

  // account change
  mixin_handlers["_account"].push(function (value) {
    console.log("logout");
    if (!value.status) {
      console.log("logout");
      document.getElementById("account").innerHTML = `
            <a href="#register">Register</a>
            <a href="#login">Login</a>
          `;
    }
  });
}

/* Shop module */

class ShopItem {
  constructor(id, name, description, price) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
  }
}

class Shopper {
  static headers = {};

  constructor() {
    this.items = [];
  }

  append_item;
}
